import React,{useState, useEffect} from 'react';
import {Card, Button, Row, Col, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import '../pages/Products.css';


export default function Product({productProp}) {

  console.log(productProp);

   return (

      <>
            <Col md={3} className="card1 mx-3">
          

            <Card>
              <Card.Img id="card-img" src={productProp.image}alt="Card image" />
              <Card.ImgOverlay className="overlay-content">
                <Card.Title className="item_title text-center">{productProp.name}</Card.Title> 
                <Link className="button_item" to={`/products/${productProp._id}`}>
                  
                  
                  <Card.Text>$ {productProp.price}</Card.Text>

                </Link>
              </Card.ImgOverlay>
            </Card>


            </Col>
     
      </>

      )

}
