import React, { useContext, useState, useEffect } from 'react';
import { Navbar, Nav, Container, Badge, Form, Button, FormControl,  Dropdown, NavDropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import logo from '../images/laraOS1.jpg';
import './AppNavBar.css';
import * as Unicons from '@iconscout/react-unicons';
import UserContext from '../userContext';
import Product from './Product';





export default function AppNavBar() {



const {user, cart} = (useContext(UserContext));

const [productsArray, setProductsArray] = useState([]);

useEffect(() => {

        fetch("https://arcane-wave-04725.herokuapp.com/products/getAllActive")
            .then(res => res.json())
            .then(data => {

                setProductsArray(data.map(product => {

                   return (

                        <Product key={product._id} productProp={product}/>

                    )
                }))

            })

    }, [])

    return (

      <Navbar expand="lg" className="navbarBG sticky-top">
  		<Navbar.Collapse id="basic-navbar-nav" className="order-lg-1 order-4 left-side">
      <Nav className="mr-auto ml-4" id="navComp">
        <Nav.Link className="navLink"  as={Link} to="/products"> SHOP</Nav.Link>
        <Nav.Link className="navLink"  as={Link} to="/viewAllGadgetsShop"> GADGETS</Nav.Link>
        <Nav.Link className="navLink"  as={Link} to="/viewAllApparelItems"> APPARELS</Nav.Link>
        <Nav.Link className="navLink" as={Link} to="/viewAllBagItems"> BAGS</Nav.Link>
      </Nav>
    </Navbar.Collapse>

    <Navbar.Brand as={Link} to="/" className="order-2">
    	<img className="navbarBrand" src={logo}/>
    </Navbar.Brand >
    {
      user.isAdmin
      ?
      <Nav.Link className="order-lg-4 order-3 navLink" as={Link} to="/cart"><Unicons.UilShoppingBag size="30" /></Nav.Link>
      :
        user.id
        ?
        <Nav.Link className="order-lg-4 order-3 navLink" as={Link} to="/cart"><span><badge className="ml-4" id="cart-badge">{cart.length}</badge><Unicons.UilShoppingBag size="40" /></span></Nav.Link>
        :
        <Nav.Link className="order-lg-4 order-3 navLink" as={Link} to="/cart"><Unicons.UilShoppingBag size="30" /></Nav.Link>
    }

    <Navbar.Toggle id="Hbutton" aria-controls="basic-navbar-nav" />
       
    <Navbar.Collapse id="basic-navbar-nav" className="order-lg-3 order-5">
      <Nav className="ml-md-auto ml-3 " id="navComp">
        <Form className="d-flex order-2 order-md-1">
        <FormControl
            type="search"
            placeholder="Search"
            className="me-2"
            aria-label="Search"
            id="searchBar"
          />
          <button id="searchButton" ><Unicons.UilSearch size="30"/></button>
        </Form>

        <NavDropdown  className="navLink order-1 order-md-2" title={<span className="navLink"><Unicons.UilUserCircle size="30"/></span>} id="basic-nav-dropdown">
        {
          user.id
          ? 
            user.isAdmin
            ?
            <>
            <NavDropdown.Item className="navLink" as={Link} to="/addProduct"><Unicons.UilPlus size="25"/> Add Product</NavDropdown.Item>
            <NavDropdown.Item className="navLink" as={Link} to="/logout"><Unicons.UilSignOutAlt size="25"/> Logout</NavDropdown.Item>
            </>
            :
            <NavDropdown.Item className="navLink" as={Link} to="/logout"><Unicons.UilSignOutAlt size="25"/> Logout</NavDropdown.Item>
          :
          <>
          <NavDropdown.Item className="navLink" as={Link} to="/register"><Unicons.UilEdit size="25"/> Register</NavDropdown.Item>
          <NavDropdown.Item className="navLink" as={Link} to="/login"><Unicons.UilSignInAlt size="25"/> Login</NavDropdown.Item>
          </>

        }
          
        </NavDropdown>
        
      </Nav>
    </Navbar.Collapse>
  </Navbar>
    )

}