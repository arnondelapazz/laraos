import React, {useEffect} from "react";
import {Container, Row, Col, Image, Nav} from 'react-bootstrap';
import gadgetPhoto from '../images/gadget.jpg';
import apparelsPhoto from '../images/apparel.jpg';
import bagsPhoto from '../images/bags.jpg';
import './Home.css';
import HomeGFeatures from './HomeGFeatures'
import HomeAFeatures from './HomeAFeatures'
import HomeBFeatures from './HomeBFeatures'
import Footer from './Footer'
import { Link } from 'react-router-dom';
import Aos from "aos";
import "aos/dist/aos.css";

export default function Home() {

	useEffect(() => {
		Aos.init({duration:2000});
	}, [])

	return (
		<>
		<Container fluid className="px-0 mb-5 home">
			<Row>
				
				<Col md={8} className="px-0" id="hp1">
					<div >
    			
					<img className="pl-0 animated fadeInLeft" id="gadget" src={gadgetPhoto} width="100%"/>
					
					<Nav.Link id="gadget-content" as={Link} to="/viewAllGadgetsShop">PRE-ORDER NOW ⮞</Nav.Link>
					</div>
				</Col>

				<Col md={4} className="px-0">
					
						<Image className="pl-0" id="apparel" src={apparelsPhoto} width="100%" fluid/>
						<Nav.Link as={Link} to="/viewAllApparelItems" id="apparel-content">VIEW COLLECTIONS NOW ⮞</Nav.Link>
				
						<Image className="pl-0" id="bags" src={bagsPhoto} width="100%"  fluid/>
						<Nav.Link as={Link} to="/viewAllBagItems" id="bags-content">VIEW COLLECTIONS NOW ⮞</Nav.Link>
				
				</Col>	
			</Row>
		</Container>
		<HomeGFeatures/>
		<HomeAFeatures/>
		<HomeBFeatures/>
		<Footer/>
		</>

		)

}