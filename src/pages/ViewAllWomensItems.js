import React, { useState, useEffect, useContext } from 'react';
import Product from '../components/Product';
import UserContext from '../userContext';
import { Table, Button, Container, Row, Col } from 'react-bootstrap';
import './Products.css';
import SideDash from './SideDash';
import { Redirect } from 'react-router-dom';

export default function ViewAllMensItems() {

    const { user } = useContext(UserContext)
    console.log(user)

    const [allWomensArray, setAllWomensArray] = useState([]);
   
    useEffect(() => {

        fetch("https://arcane-wave-04725.herokuapp.com/products/viewAllWomensItem")
            .then(res => res.json())
            .then(data => {

                setAllWomensArray(data.map(product => {

                    return (

                        <Product key={product._id} productProp={product}/>

                    )

                }))

            })

    }, [])


    console.log(allWomensArray);

    return ( 
        user.isAdmin
        ?
        <Redirect to="/products"/>
        :
    	<>
		<Container fluid>
        <Row>
        <Col md={3} className="mt-5 carousel2">
		
		
			<h1 className="shop-title text-center">Ladies Collection</h1>
             <SideDash/>
        </Col>
        <Col md={9} className="mt-5">
			<Row>
			{allWomensArray}
			</Row>
        </Col>    
        </Row>    
		</Container>
		</>

    )



}