import React, { useState, useEffect, useContext } from 'react';
import Product from '../components/Product';
import SideDash from './SideDash';
import UserContext from '../userContext';
import { Table, Button, Container, Row, Col, Form, Nav } from 'react-bootstrap';
import './Products.css';
import { Link } from 'react-router-dom';
import * as Unicons from '@iconscout/react-unicons';


export default function Products() {

    const { user } = useContext(UserContext)
    console.log(user)

    const [productsArray, setProductsArray] = useState([]);
    const [allProducts, setAllProducts] = useState([]);
    const [update, setUpdate] = useState(0);

    useEffect(() => {

        fetch("https://arcane-wave-04725.herokuapp.com/products/getAllActive")
            .then(res => res.json())
            .then(data => {

                setProductsArray(data.map(product => {

                    return (

                        <Product key={product._id} productProp={product}/>

                    )

                }))

            })

    }, [])

    useEffect(() => {

        if (user.isAdmin) {

            fetch("https://arcane-wave-04725.herokuapp.com/products/getAllProducts", {

                    headers: {
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    }

                })
                .then(res => res.json())
                .then(data => {

                    console.log(data)
                    setAllProducts(data.map((product) => {


                        return (

                            <tr key={product._id}>
							<td className="text-center">{product._id}</td>
							<td className="text-center">{product.name}</td>
							<td className="text-center">{product.price}</td>
							<td className="text-center">{product.isActive ? "Active": "Inactive"}</td>
							<td className="text-center">
								{
									product.isActive
									?
										<button id="archive" className="mx-2" onClick={()=>{
											
											archive(product._id)
										}}>Archive
										</button>
									:
										<button id="activate" className="mx-2" onClick={()=>{
											
											activate(product._id)
										}}>
											Activate
										</button>

								}
							</td>
						</tr>


                        )

                    }))

                })

        }



    }, [user, update])

    function archive(productId) {

        console.log(productId)

        fetch(`https://arcane-wave-04725.herokuapp.com/products/archiveProduct/${productId}`, {

                method: 'PUT',
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)
              
                setUpdate(update + 1)
            })

    }

    function activate(productId) {

        fetch(`https://arcane-wave-04725.herokuapp.com/activateProduct/${productId}`, {

                method: 'PUT',
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                setUpdate(update + 1)
            })

    }

    console.log(productsArray);

    return ( 

    	user.isAdmin
		?
		<>	
			<Container>
			<h1 className="shop-title text-center mt-5 mb-3">ADMIN DASHBOARD</h1>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th className="text-center">ID</th>
						<th className="text-center">Name</th>
						<th className="text-center">Price</th>
						<th className="text-center">Status</th>
						<th className="text-center">Actions</th>
					</tr>
				</thead>
				<tbody>
					
					{allProducts}
				</tbody>
			</Table>
			</Container>
		</>	
		:
		<>
		<Container fluid>
		<Row>
			<Col md={3} className="mt-5 carousel2">
				<h1 className="shop-title text-center">OUR ITEMS</h1>
				<SideDash/>
				
			</Col>
		
			<Col md={9} className="mt-5">
				<Row>
			{productsArray}
			</Row>	
			</Col>
			
		</Row>
		</Container>
		</>

    )



}