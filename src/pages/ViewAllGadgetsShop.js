import React, { useState, useEffect, useContext } from 'react';
import Product from '../components/Product';
import UserContext from '../userContext';
import { Table, Button, Container, Row, Col } from 'react-bootstrap';
import './Products.css';
import SideDash from './SideDash';
import { Redirect } from 'react-router-dom';

export default function ViewAllGadgetsShop() {

    const { user } = useContext(UserContext)
    console.log(user)

    const [allGadgetsArray, setAllGadgetsArray] = useState([]);
   
    useEffect(() => {

        fetch("https://arcane-wave-04725.herokuapp.com/products/viewAllGadgetsItem")
            .then(res => res.json())
            .then(data => {

                setAllGadgetsArray(data.map(product => {

                    return (

                        <Product key={product._id} productProp={product}/>

                    )

                }))

            })

    }, [])

   

   

    console.log(allGadgetsArray);

    return ( 
        user.isAdmin
        ?
        <Redirect to="/products"/>
        :
        
        <>
        <Container fluid>
        <Row>
        <Col md={3} className="mt-5 carousel2">
            <h1 className="shop-title text-center">GADGETS COLLECTION</h1>
            <SideDash/>
        </Col>
        <Col md={9} className="mt-5">
            <Row>
            {allGadgetsArray}
            </Row>
        </Col>    
        </Row>      
        </Container>
        </>

    )



}
