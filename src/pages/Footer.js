import React from "react";
import { Container, Carousel, Card, Col, Row, Form } from 'react-bootstrap';
import './Footer.css';
import * as Unicons from '@iconscout/react-unicons';

export default function Footer() {

	return (
		<Container fluid className="footer mt-2 pt-5">
		<Container className="footer2">
			<Col md={12}>
			<h3 className="text-center followUs">FOLLOW & MESSAGE US ON:</h3>
			<div className="text-center"><Unicons.UilInstagramAlt size="50" /> <Unicons.UilFacebookF size="50" /> <Unicons.UilFacebookMessenger size="50" /></div>
			</Col>
			<Col lg={12}>
		        <h3 className="text-center copyR pb-3 pt-2">© Copyright 2021 LARA'S ONLINE SHOP</h3>          
		     </Col>
		</Container>
		</Container>	

		)

}
