import React, { useState, useEffect, useContext } from 'react';
import { Card, Button, Row, Col, Container, InputGroup, Form, Nav} from 'react-bootstrap';
import { useParams, Link, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext';
import './ViewProduct.css';
import * as Unicons from '@iconscout/react-unicons';

export default function ViewProduct() {


    const { productId } = useParams();

    const { user, cart, setCart } = useContext(UserContext)



    const [productDetails, setProductDetails] = useState({

        id: null,
        name: null,
        image: null,
        price: null,
        description: null


    })

    useEffect(() => {

        fetch(`https://arcane-wave-04725.herokuapp.com/products/getSingleProduct/${productId}`)
            .then(res => res.json())
            .then(data => {

                console.log(data)

                if (data.message) {

                    Swal.fire({

                        icon: "error",
                        title: "Product Unavailable",
                        text: data.message

                    })

                } else {

                    setProductDetails({

                        id: data._id,
                        name: data.name,
                        image: data.image,
                        price: data.price,
                        description: data.description


                    })

                }

            })

    }, [productId])

    //console.log(productDetails)
    //console.log(cart)

    const addToCart = () => {

        //console.log(cart)
        //console.log(productDetails.name)
        let qty = productDetails.quantity = 1
        // console.log(productDetails)
        let findItem = cart.find(i => i.id === productDetails.id)

        if (findItem) {
            Swal.fire({

                icon: "error",

                title: "ITEM ALREADY ADDED TO CART",


            })

            


        } else {
            setCart([...cart, productDetails])

            Swal.fire({

                icon: "success",
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                },
                title: "ITEM ADDED TO CART",


            })


        }

        return cart



    }

    const removeCart = () => {


        let filter = cart.filter(i => i.id !== productDetails.id)
        let findItem = cart.find(i => i.id === productDetails.id)

        if (findItem) {

                setCart(filter)

                      Swal.fire({

            icon: "success",
            showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                },
            title: "ITEM REMOVED TO CART",

 })

        } else { Swal.fire({

                icon: "error",
                title: "NO ITEM",


            })}

        

                                
  


        return cart


    }

    //console.log(cart)
    //console.log(setCart)

    return (

        <Container id="container-card">       
           
               

                   
                    <Row id="cart-card">
                   
                    <Col md={6}>
                      <Card.Img id="img-cart" src={productDetails.image} />
                     
                      </Col>
                      <Col md={6}>
                      <Card.Body>
                      
                      
                        <Card.Title id="product-name">{productDetails.name}</Card.Title>
                        <Card.Text id="product-price">$ {productDetails.price}</Card.Text>
                        <Card.Text id="product-desc">{productDetails.description}</Card.Text>
                       
                         {
                            user.isAdmin === false 
                            ?
                            cart.find(i => i.id === productDetails.id)
                                ?
                                 <>
                                <button className="hide"  type="submit" disable></button>
                                <button className="remove"  type="submit" onClick={()=>removeCart()}>REMOVE ITEM</button >
                                </>
                                :
                                <>
                                <button className="add" type="submit" onClick={()=>addToCart()}>ADD TO CART</button >
                                <button className="hide"  type="submit" disable></button>
                                </>
                            :
                                <Link className="add" to="/login">ADD TO CART</Link>
                         }
                     </Card.Body>

                        
                       
                      </Col>
                     
                      </Row> 
                   


                
           
    </Container>

    )



}