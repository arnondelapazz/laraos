import React,{useEffect} from "react";
import { Container, Carousel, Card, Col, Row, Form, Image } from 'react-bootstrap';
import bag1 from '../images/bag1.png';
import bag2 from '../images/bag2.png';
import bag3 from '../images/bag3.png';
import './HomeBFeatures.css';
import * as Unicons from '@iconscout/react-unicons';
import Aos from "aos";
import "aos/dist/aos.css";

export default function Home() {

  useEffect(() => {
    Aos.init({duration:1400});
  }, [])

    return (
    	<Container fluid className="bagSec bagss mt-5 pt-5">
      <Row>
      <Col lg={12} id="c1st">
        <Carousel fade id="carousel1">
 	        <Carousel.Item>
            <Image
              fluid
              className="d-block bags"
              src={bag1}
              alt="First slide"
            />
          </Carousel.Item>
          <Carousel.Item>
            <Image
              fluid
              className="d-block bags"
              src={bag3}
              alt="Second slide"
            />
          </Carousel.Item>
          <Carousel.Item>
            <Image
              fluid
              className="d-block bags"
              src={bag2}
              alt="Second slide"
            />
          </Carousel.Item>
      </Carousel>
      </Col>
      <Col lg={12} id="email2" className="mt-4 pt-4 d-md-flex d-none">
      <Card id="subscription-email">
      <Row className="px-4">
      <Col md={6} className="content01">
        <Card.Title className="subtitle1"><Unicons.UilEnvelopeAlt size="40" />   S T A Y&nbsp;&nbsp;U P&nbsp;&nbsp;T O&nbsp;&nbsp;D A T E</Card.Title>         
      </Col>  
      <Col md={4} className="content02">

       <Form.Group>
        <Form.Control id="input-subs" type="email"  placeholder="" required/>
      </Form.Group>
              
      </Col>
      <Col md={2} className="content03">
         <button className="btn-subs">SUBSCRIBE</button>
      </Col> 
      </Row>   
      </Card>
      </Col>
      </Row>
    </Container>
    )

}