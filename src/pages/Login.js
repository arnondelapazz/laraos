import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Col, Row, Container,Nav } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../userContext';
import { Redirect, Link  } from 'react-router-dom';
import './Register.css';
import Aos from "aos";
import "aos/dist/aos.css";


export default function Login() {

    useEffect(() => {
        Aos.init({duration:2000});
    }, [])


    const { user, setUser } = useContext(UserContext);

    const [email, inputEmail] = useState("");
    const [password, inputPassword] = useState("");
    const [isActive, inputIsActive] = useState(false);

    useEffect(() => {

        if (email !== "" && password !== "") {

            inputIsActive(true)

        } else {

            inputIsActive(false)

        }

    }, [email, password])

    function loginUser(e) {

        e.preventDefault();


        fetch('https://arcane-wave-04725.herokuapp.com/users/login', {

                method: 'POST',
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })

            })
            .then(res => res.json())
            .then(data => {


                if (data.accessToken) {


                    Swal.fire({

                        icon: "success",
                        title: "Login Succesful",
                        showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                },
                        text: `Thank you for logging in!`

                    })

                    localStorage.setItem('token', data.accessToken)

                    fetch('https://arcane-wave-04725.herokuapp.com/users/getUserDetails', {

                            headers: {

                                'Authorization': `Bearer ${localStorage.getItem('token')}`

                            }

                        })
                        .then(res => res.json())
                        .then(data => {

                            setUser({

                                id: data._id,
                                isAdmin: data.isAdmin

                            })

                        })

                } else {

                    Swal.fire({

                        icon: "error",
                        title: "Login Failed.",
                         showClass: {
                    popup: 'animate__animated animate__shakeY'
                },
                        text: data.message

                    })

                }

            })


    }

    return (

		user.id
		?
		<Redirect to="/products"/>
		:
		<>
        <Container className="register" >
        <Row className="row1" >
        <Col md={7} className="input" data-aos="slide-left">
        <Form onSubmit={e => loginUser(e)}>
		<h1 className="mb-3 text-center create">SIGN IN</h1>
        <Row className="input">
		
            <Col md={12}>
			<Form.Group controlId="userEmail">
				<Form.Control type="email" value={email} onChange={e => {inputEmail(e.target.value)}} placeholder="Email Address" required/>
			</Form.Group>
            </Col>
            <Col md={12} className="mt-3">
			<Form.Group controlId="userPassword">
				<Form.Control type="password" value={password} onChange={e => {inputPassword(e.target.value)}} placeholder="Password" required/>
			</Form.Group>
            </Col>
            <Col className="mt-3 text-center" >
			{

				isActive
				
				? <button className="btn-2" type="submit">SIGN IN</button>
				: <button className="btn-2" disabled>SIGN IN</button>

			}
            	
            </Col>	
            </Row>

		</Form>
        </Col>
        <Col md={5} className="d-none d-md-block div2" data-aos="slide-right">
         <div className="r1">
            <h1 className="mt-1 text-center title1">Hello, Shopper!</h1>

            <p className="subinf text-center">Enter your personal details and start shopping!</p>
            <Nav.Link className="button-1 text-center" as={Link} to="/register">SIGN UP</Nav.Link>
         </div>
            
        </Col>
        </Row>
        </Container>
		</>

		)

}