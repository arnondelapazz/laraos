import React, {useEffect} from "react";
import {Container, Row, Col, Image, Card} from 'react-bootstrap';
import gProd1 from '../images/ip132.jpg';
import gProd2 from '../images/app3.jpg';
import gProd3 from '../images/gProd-3.jpg';
import './HomeGFeatures.css';
import {Link} from 'react-router-dom';
import Aos from "aos";
import "aos/dist/aos.css";


export default function HomeGFeatures() {

	useEffect(() => {
		Aos.init({duration:1500});
	}, [])

	return (

		<Container fluid className="homeFeatures mx-0 px-0 pt-5 pb-3">
			<Row>
			
			<Col md={4} id="gItem1">
			<Card  id="display-card1">
			<Card.Img className="display-img" src={gProd1}alt="Card image" />
				<Card.ImgOverlay id="overlay1">
				<Link className="name1" to={'/viewAllGadgetsShop'}>
                <Card.Title className="item_title text-center">MOBILE PHONES</Card.Title> 
                </Link>
              	</Card.ImgOverlay>	
			</Card>
			</Col>
		
			<Col md={4} id="gItem2">
			<Card id="display-card2">
			<Card.Img className="display-img" src={gProd2}alt="Card image" />
				<Card.ImgOverlay id="overlay2">
				<Link className="name1" to={'/viewAllGadgetsShop'}>
                <Card.Title className="item-ttle text-center">ACCESSORIES</Card.Title>
                </Link> 
              	</Card.ImgOverlay>	
			</Card>
			</Col>
			<Col md={4} id="gItem3">
			<Card id="display-card3">
			<Card.Img className="display-img" src={gProd3}alt="Card image" />
				<Card.ImgOverlay id="overlay3">
				<Link className="name1" to={'/viewAllGadgetsShop'}>
                <Card.Title className="item_title text-center">MOBILE TABLETS</Card.Title> 
                </Link>
              	</Card.ImgOverlay>	
			</Card>
			</Col>

			</Row>
		</Container>

		)

}