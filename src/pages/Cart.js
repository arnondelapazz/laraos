import React, { useState,  useContext, useEffect } from 'react';
import { Card, Button, Row, Col, Container, InputGroup, Form, Table } from 'react-bootstrap';
import { useParams, useHistory, Link, Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext';
import './Cart.css';
import * as Unicons from '@iconscout/react-unicons';


export default function Cart() {

    let { user, cart, setCart } = useContext(UserContext)

    const [total, setTotal] = useState(0);



    useEffect(() => {

    	setTotal(cart.reduce(function(a,c) {
    	 	return (a + c.price * c.quantity)
    	 }, 0))

    },[cart])

    

 
  	   const addQty = (cartId) => {

  	   let newQty = cart.map(item => {return item})
	
			newQty.forEach(function(obj) {
				if (obj.id === cartId){
					obj.quantity += 1

				}
			})
  	
    	setCart(newQty)  

    	
    }

   const subQty = (cartId) => {

    	 let newQty = cart.map(item => {return item})
	
			newQty.forEach(function(obj) {
				if (obj.id === cartId){
					obj.quantity -= 1

				}
			})
  	
    	setCart(newQty)  

       

    }


    const removeCart = (cartId) => {

        let filter = cart.filter(i => i.id !== cartId)
        let findItem = cart.find(i => i.id === cartId)

        if (findItem){

        	setCart(filter)

        	Swal.fire({

            icon: "success",
            showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                },
            title: "ITEM REMOVED TO CART",

 })

        }
                      
        return cart

    }


    const checkout = () => {


		 fetch('https://arcane-wave-04725.herokuapp.com/orders/checkout', {

                    method: 'POST',
                    headers: {
                        "Content-Type": "application/json",
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                    	userId: user._id,
                    	products: cart,
                    	totalAmount: total
                    })

                })
		 		.then(res => res.json())
		 		.then(data => {

		 			console.log(data)


		 			if(data.message === "Checkout Complete! Thank you for purchasing!") {

		 				Swal.fire({
		 					icon: 'success',
		 					title: "CHECKOUT COMPLETE",
		 					text: `Thank you for Purchasing!`
		 				});
		 				
		 				setCart([])
		 				setTotal(0)
		 				

		 			} else {

		 				Swal.fire({
		 					icon: 'error',
		 					title: "Something went wrong",
		 				
		 				});

		 			}

		 		

		 		})

	}



    return (
        user.isAdmin === false ?

        <Container>		
	 	 	
	 	 	<Container className="">
			<Row >
					<Col md={8}>
						<h1  className="shop-title mt-5">Shopping Cart</h1>
                		<Table hover>
							<thead>
								<tr>
									<th className="thead text-center">Image</th>
									<th className="thead text-center">Name</th>
									<th className="thead text-center">Price</th>
									<th className="thead text-center">Quantity</th>
									<th className="thead text-center">Total</th>
									<th className="thead text-center">Actions</th>
								</tr>
							</thead>
							
				
					 {cart.map((val, key) => {
                  	return(
                  		<>
                  			<tbody>	
                  		    <tr>
								<th className="text-center"><img className="cart-imgs" src={val.image}/></th>
			                  	<th className="tth text-center">{val.name}</th>
			                  	<th  className="tth text-center">$ {val.price}</th>
			                  	<th className="tth text-center">
			                  	{
			                  		val.quantity === 1 
			                  		?
			                  		<button className="operator" onClick={()=>subQty(val.id)} disabled>-</button>
			                  		:
			                  		<button className="operator" onClick={()=>subQty(val.id)}>-</button>
			                  	}
				                  
				                  	
				                  		<input type="text" id="qty-cart" value={val.quantity}/>
				                  		
				                  	
				                  	<button className="operator" onClick={()=>addQty(val.id)}>
				                  		+
				                  	</button>
			                  	</th>
			                  	<th  className="tth text-center">$ {val.price * val.quantity}</th>
			                  	<th className="text-center"><button className="remove" onClick={()=>removeCart(val.id)}><Unicons.UilTrashAlt  size="30" /></button></th>
                  			</tr>
                  			
 						</tbody>

						    
										
                  		</>
                  		) 
                  })} 
				
						</Table>
 						
               	</Col> 
				<Col md={4}>  
						<h1 className="text-center mt-5 orders shop-title">Order Summary</h1>
	 						<Table>
	 					
							<tr>
								<th className="pl-0">PROMO CODE</th>
							</tr>
							<tr>
								<input className="promocode" type="text" placeholder="Enter your code"/>
							</tr>
							<tr>
								<button className="pcode mt-3">APPLY</button>
							</tr>
	 						</Table>
	 						<Table>
	 							<tr>
	 								<th className="pl-0">TOTAL COST</th>
	 								<th className="totals">$ {total}</th>
	 							</tr>
	 							
	 						</Table>
	 						
							<button className="checkout mt-3" onClick={()=>checkout()}>CHECKOUT</button>
							
	 						
	 						
 						</Col> 

				
				
				
			</Row>
			</Container>	
	</Container> :
        <Redirect to="/login"/>
    )

}

					